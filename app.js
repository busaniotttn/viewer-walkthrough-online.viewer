{
    "name": "view-waltrough-online.viewer",
    "description": "Transparent use of Model Derivative to view Box compatible files.",
    "repository": "https://bitbucket.org/busaniotttn/viewer-walkthrough-online.viewer",
    "logo": "https://avatars0.githubusercontent.com/u/8017462?v=3&s=200",
    "keywords": [
      "express",
      "framework",
      "autodesk",
      "forge",
      "template"
    ],
    "env": {
      "FORGE2_CLIENT_ID": {
        "description": "Forge Client ID"
      },
      "FORGE2_CLIENT_SECRET": {
        "description": "Forge Client Secret"
      }
    },
    "website": "https://developer.autodesk.com/",
    "success_url": "/"
  }